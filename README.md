# Site Guardian Server Benchmarks module

Site Guardian Server Benchmarks adds the ability to run some some simple benchmarks and view summarized results to the site Status Report.

It provides additional information to the Status report detailing the results of the last time the benchmarks it provides were run.

Benchmarks categories provided...

- PHP - Math, String, Loops, If/Else, Prime Sieve, N-Queens.
- Database - Connect, Select, Query and benchmark.
- File IO - Read, Write Zip and Unzip

It is important to note that the module does not run the benchmarks in response to the running of the site status report or any call to the Site Guardian API. The benchmarks can be run by visiting admin->reports->Server benchmarks. It is the results of visiting this page that are reported in the Site Status report and returned to the Site Guardian API.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/sgd_server_benchmarks).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/sgd_server_benchmarks).

## Table of contents

- Recommended modules
- Installation
- Configuration
- Maintainers

## Recommended modules

Site Guardian Server Benchmarks is a companion module for the Site Guardian (API) module and as such will add to the information returned to a consumer of the Site Guardian (API).

See the Site Guardian [project page](https://www.drupal.org/project/site_guardian).

If used in conjunction with the Site Guardian API it returns additional information when a consumer of the Site Guardian (API) requests the sites information.

At present the module return results of the last time the benchmarks where run.

Although part of the Site Guardian ecosystem it provides functionality that is useful irrespective of whether the Site Guardian (API) is installed or not and does not require the Site Guardian (API) module to function.

## Installation

Install as you would normally install a contributed Drupal module. For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

The module has no menu or modifiable settings. There is no configuration. When
enabled, the module will add information to the Site Status report as described above.

## Maintainers

- Andy Jones - [arcaic](https://www.drupal.org/u/arcaic)
