<?php

namespace Drupal\sgd_server_benchmarks;

/**
 * Class implementing the PHP language benchmarks.
 */
class PhpBenchmarks extends Benchmarks {

  /**
   * Returns the formatted results for the benchmarks.
   */
  public function getResults($iterations) {

    $result = [];

    $timeStart = microtime(TRUE);

    $result = [
      'math' => [
        'title' => $this->t('Math operations'),
        'description' => $this->t('PHP floating point math operations (Total iterations = &lt;iterations&gt; * 750,000).'),
        'result' => self::benchmarkMath($iterations * 750000),
      ],
      'string' => [
        'title' => $this->t('String operations'),
        'description' => $this->t('PHP String operations (Total iterations = &lt;iterations&gt; * 275,000).'),
        'result' => self::benchmarkString($iterations * 275000),
      ],
      'loops' => [
        'title' => $this->t('Loops'),
        'description' => $this->t('PHP loop functions (Total iterations = &lt;iterations&gt; * 17,500,000).'),
        'result' => self::benchmarkLoops($iterations * 17500000),
      ],
      'ifelse' => [
        'title' => $this->t('If Else'),
        'description' => $this->t('PHP If/Else (Total iterations = &lt;iterations&gt; * 14,000,000).'),
        'result' => self::benchmarkIfelse($iterations * 14000000),
      ],
      'sieve' => [
        'title' => $this->t('Prime sieve'),
        'description' => $this->t('Sieve of eratosthenes. (@link)', ['@link' => 'https://github.com/PlummersSoftwareLLC/Primes']),
        'result' => self::benchmarkPrimeSieve($iterations),
      ],
      'matmul' => [
        'title' => $this->t('Matrix multiplicaiton'),
        'description' => $this->t('Multiplying two square matrices of 200x200 in size. (@link)', ['@link' => 'https://github.com/attractivechaos/plb2']),
        'result' => self::benchmarkMatMul($iterations),
      ],
      'nqueen' => [
        'title' => $this->t('N Queen'),
        'description' => $this->t('N Queens problem n = 12 (@link)', ['@link' => 'https://github.com/attractivechaos/plb2']),
        'result' => self::benchmarkEnQueen($iterations),
      ],
      'total' => [
        'title' => $this->t('Total'),
        'description' => '',
        'result' => self::timerDiff($timeStart),
      ],

    ];

    return $result;
  }

  /**
   * Runs the math function benchmarks 'count' number of times.
   */
  public static function benchmarkMath($count) {

    $timeStart = microtime(TRUE);

    for ($i = 0; $i < $count; $i++) {
      sin($i);
      asin($i);
      cos($i);
      acos($i);
      tan($i);
      atan($i);
      abs($i);
      floor($i);
      exp($i);
      is_finite($i);
      is_nan($i);
      sqrt($i);
      log10($i);
    }

    return self::timerDiff($timeStart);
  }

  /**
   * Runs the string function benchmarks 'count' number of times.
   */
  public static function benchmarkString($count) {

    $timeStart = microtime(TRUE);

    $string = 'the quick brown fox jumps over the lazy dog';

    for ($i = 0; $i < $count; $i++) {
      addslashes($string);
      chunk_split($string);
      metaphone($string);
      strip_tags($string);
      md5($string);
      sha1($string);
      strtoupper($string);
      strtolower($string);
      strrev($string);
      strlen($string);
      soundex($string);
      ord($string);
    }

    return self::timerDiff($timeStart);
  }

  /**
   * Runs the loops benchmarks 'count' number of times.
   */
  public static function benchmarkLoops($count) {

    $timeStart = microtime(TRUE);

    for ($i = 0; $i < $count; ++$i) {
    }

    $i = 0;

    while ($i < $count) {
      ++$i;
    }

    return self::timerDiff($timeStart);
  }

  /**
   * Runs the if/else benchmarks 'count' number of times.
   */
  public static function benchmarkIfelse($count) {

    $a = 0;

    $timeStart = microtime(TRUE);

    for ($i = 0; $i < $count; $i++) {
      if ($i == -1) {
        $a++;
      }
      elseif ($i == -2) {
        $a++;
      }
      else {
        if ($i == -3) {
          $a++;
        }
      }
    }

    return self::timerDiff($timeStart);
  }

  /**
   * Runs the PrimSieve benchmarks 'count' number of times.
   */
  public static function benchmarkPrimeSieve($count) {

    $sieveSize = 6200000;

    $timeStart = microtime(TRUE);

    $sieve = new PrimeSieve($sieveSize);

    for ($i = 0; $i < $count; $i++) {

      // Run the sieve.
      $sieve->runSieve();

      // Get the result.
      $sieve->getRawbitCount();
    }

    return self::timerDiff($timeStart);
  }

  /**
   * Runs the matrix multiplication benchmarks 'count' number of times.
   */
  public static function benchmarkMatMul($count) {

    $timeStart = microtime(TRUE);

    $matmul = new MatMul();

    $n = 200;

    for ($i = 0; $i < $count; $i++) {

      $a = $matmul->doMatGen($n);
      $b = $matmul->doMatGen($n);

      $c = $matmul->doMatMul($n, $a, $b);
    }

    return self::timerDiff($timeStart);
  }

  /**
   * Runs the NQueens benchmarks 'count' number of times.
   */
  public static function benchmarkEnQueen($count) {

    $timeStart = microtime(TRUE);

    $nQueen = new NQueen();

    $n = 12;

    for ($i = 0; $i < $count; $i++) {
      $m = $nQueen->nqSolve($n);
    }

    return self::timerDiff($timeStart);
  }

}
