<?php

namespace Drupal\sgd_server_benchmarks;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class implementing the benchmark base class.
 */
class Benchmarks {

  use StringTranslationTrait;

  const RANDSTRING = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";

  /**
   * Returns the time difference between now and time passed.
   */
  public static function timerDiff($timeStart) {
    return number_format(microtime(TRUE) - $timeStart, 3);
  }

  /**
   * Generates a random string of the requested length.
   */
  public static function generateString($length, $seed = 0) {

    if ($seed != 0) {
      mt_srand($seed);
    }

    $inputLength = strlen(self::RANDSTRING);
    $randomString = '';

    for ($i = 0; $i < $length; $i++) {
      $randomCharacter = self::RANDSTRING[mt_rand(0, $inputLength - 1)];
      $randomString .= $randomCharacter;
    }

    return $randomString;
  }

}
