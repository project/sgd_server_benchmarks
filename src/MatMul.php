<?php

namespace Drupal\sgd_server_benchmarks;

/**
 * Class implementing the matrix multiplication benchmark.
 */
class MatMul {

  /**
   * Runs the matrix generation  benchmark 'n' number of times.
   */
  public function doMatGen(int $n) {

    $a = array_fill(0, $n, array_fill(0, $n, 0));

    $tmp = 1.0 / $n / $n;

    for ($i = 0; $i < $n; ++$i) {
      for ($j = 0; $j < $n; ++$j) {
        $a[$i][$j] = $tmp * ($i - $j) * ($i + $j);
      }
    }

    return $a;
  }

  /**
   * Runs the matrix multiplication benchmark 'n' number of times.
   */
  public function doMatMul(int $n, $a, $b) {

    $c = array_fill(0, $n, array_fill(0, $n, 0));

    for ($i = 0; $i < $n; ++$i) {

      for ($k = 0; $k < $n; ++$k) {

        $aik = $a[$i][$k];
        $bk = $b[$k];

        for ($j = 0; $j < $n; ++$j) {
          $c[$i][$j] += $aik * $bk[$j];
        }
      }
    }

    return $c;
  }

}
