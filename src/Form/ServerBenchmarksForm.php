<?php

namespace Drupal\sgd_server_benchmarks\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\sgd_server_benchmarks\DbBenchmarks;
use Drupal\sgd_server_benchmarks\IoBenchmarks;
use Drupal\sgd_server_benchmarks\PhpBenchmarks;

/**
 * Class ServerBenchmarksForm. The server benchmarks report.
 *
 * @package Drupal\sgd_server_benchmarks\Form
 */
class ServerBenchmarksForm extends FormBase {

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'sgd_server_benchmarks_report';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    set_time_limit(120);

    if (!empty($iterations = $form_state->getValue('iterations'))) {

      // Get the PHP benchmarks.
      $phpResults = (new PhpBenchmarks())->getResults($iterations);

      // Output PHP benchmarks as a table.
      $header = [$this->t('Benchmarks (PHP)'), $this->t('Description'), $this->t('Time (seconds)')];

      $data = [];

      foreach ($phpResults as $key => $benchmark) {
        $data[] = [
          $benchmark['title'], $benchmark['description'], $benchmark['result'],
        ];
      }

      $form[] = [
        '#theme' => 'table',
        '#header' => $header,
        '#rows' => $data,
      ];

      // Get the DB benchmarks.
      if ($dbResults = (new DbBenchmarks())->getResults($iterations)) {

        // Output DB benchmarks as a table.
        $header = [$this->t('Benchmarks (DB)'), $this->t('Description'), $this->t('Time (seconds)')];

        $data = [];

        foreach ($dbResults as $key => $benchmark) {
          $data[] = [
            $benchmark['title'], $benchmark['description'], $benchmark['result'],
          ];
        }

        $form[] = [
          '#theme' => 'table',
          '#header' => $header,
          '#rows' => $data,
        ];

      } else {

        $form[] = [
          '#markup' => '<p>' . $this->t('Database benchmarks are unavailable. Check if the PHP mysqli extension is installed.') . '</p>',
        ];

      }

      // Get the Io benchmarks.
      $ioResults = (new IoBenchmarks())->getResults($iterations);

      // Output IO benchmarks as a table.
      $header = [$this->t('Benchmarks (IO)'), $this->t('Description'), $this->t('Time (seconds)')];

      $data = [];

      foreach ($ioResults as $key => $benchmark) {
        $data[] = [
          $benchmark['title'], $benchmark['description'], $benchmark['result'],
        ];
      }

      $form[] = [
        '#theme' => 'table',
        '#header' => $header,
        '#rows' => $data,
      ];

      // Save results to module config so can be used by SG API without running
      // everytime.
      $config = \Drupal::configFactory()->getEditable('sgd_server_benchmarks.config');

      $config
        ->set('settings', [
          'iterations' => $iterations,
          'timestamp' => time(),
        ])
        ->set('php', $phpResults)
        ->set('db', $dbResults)
        ->set('io', $ioResults)
        ->save();
    }

    $form['iterations'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Iterations'),
      '#required' => FALSE,
      '#default_value' => 1,
    ];

    $form['actions']['#type'] = 'actions';

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Run'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild(TRUE);
  }

}
