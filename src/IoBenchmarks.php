<?php

namespace Drupal\sgd_server_benchmarks;

/**
 * Class implementing the file input/output benchmarks.
 */
class IoBenchmarks extends Benchmarks {

  /**
   * Location of a temporary folder to use when performing IO operations.
   *
   * @var string
   */
  private $tempFolder;

  /**
   * A rendom string used as data in the file io becnhmarks.
   *
   * @var string
   */
  private $fileString;

  /**
   * Returns the formatted results for the benchmarks.
   */
  public function getResults($iterations) {

    $this->tempFolder = \Drupal::service('file_system')->getTempDirectory();
    $this->fileString = $this->generateString(1024 * 20, 66225533);

    $result = [];

    $timeStart = microtime(TRUE);

    $result = [
      'file_write' => [
        'title' => $this->t('File write'),
        'description' => $this->t('Write 20k file to temporary folder (Total iterations = &lt;iterations&gt; * 1,000).'),
        'result' => self::benchmarkFileWrite($iterations * 1000),
      ],
      'file_read' => [
        'title' => $this->t('File read'),
        'description' => $this->t('Read 20k file from temporary folder (Total iterations = &lt;iterations&gt; * 1,000).'),
        'result' => self::benchmarkFileRead($iterations * 1000),
      ],
      'file_zip' => [
        'title' => $this->t('Zip'),
        'description' => $this->t('Zip 20k file in temporary folder (Total iterations = &lt;iterations&gt; * 1,000).'),
        'result' => self::benchmarkFileZip($iterations * 1000),
      ],
      'file_nzip' => [
        'title' => $this->t('Unzip'),
        'description' => $this->t('Unzip 20k file into temporary folder (Total iterations = &lt;iterations&gt; * 1,000).'),
        'result' => self::benchmarkFileUnzip($iterations * 1000),
      ],
      'total' => [
        'title' => $this->t('Total'),
        'description' => '',
        'result' => self::timerDiff($timeStart),
      ],

    ];

    return $result;
  }

  /**
   * Runs the file read benchmark 'count' number of times.
   */
  private function benchmarkFileRead($count) {

    $testFileName = $this->tempFolder . '/test.txt';

    $timeStart = microtime(TRUE);

    file_put_contents($testFileName, $this->fileString);

    for ($i = 0; $i < $count; $i++) {
      file_get_contents($testFileName);
    }

    unlink($testFileName);

    return self::timerDiff($timeStart);
  }

  /**
   * Runs the file write benchmark 'count' number of times.
   */
  private function benchmarkFileWrite($count) {

    $testFileName = $this->tempFolder . '/test.txt';

    $timeStart = microtime(TRUE);

    for ($i = 0; $i < $count; $i++) {
      file_put_contents($testFileName, $this->fileString);
    }

    unlink($testFileName);

    return self::timerDiff($timeStart);
  }

  /**
   * Runs the file Zip benchmark 'count' number of times.
   */
  private function benchmarkFileZip($count) {

    $testFileName = $this->tempFolder . '/test.txt';
    $testZipName = $this->tempFolder . '/test.zip';

    $timeStart = microtime(TRUE);

    file_put_contents($testFileName, $this->fileString);

    for ($i = 0; $i < $count; $i++) {

      $zip = new \ZipArchive();

      $zip->open($testZipName, \ZipArchive::CREATE);
      $zip->addFile($testFileName);
      $zip->close();
    }

    unlink($testFileName);
    unlink($testZipName);

    return self::timerDiff($timeStart);
  }

  /**
   * Runs the file unzip benchmark 'count' number of times.
   */
  private function benchmarkFileUnzip($count = 1000) {

    $testFileName = $this->tempFolder . '/test.txt';
    $testZipName = $this->tempFolder . '/test.zip';
    $unzipFolder = $this->tempFolder . '/test';

    $timeStart = microtime(TRUE);

    file_put_contents($testFileName, $this->fileString);

    $zip = new \ZipArchive();

    $zip->open($testZipName, \ZipArchive::CREATE);
    $zip->addFile($testFileName);
    $zip->close();

    for ($i = 0; $i < $count; $i++) {

      $zip = new \ZipArchive();

      $zip->open($testZipName);
      $zip->extractTo($unzipFolder);
      $zip->close();
    }

    unlink($testFileName);
    unlink($testZipName);

    $this->rrmdir($unzipFolder);

    return self::timerDiff($timeStart);
  }

  /**
   * Runs the file recursive remove benchmark 'count' number of times.
   */
  public function rrmdir($dir) {

    if (is_dir($dir)) {

      $objects = scandir($dir);

      foreach ($objects as $object) {
        if ($object != "." && $object != "..") {
          if (is_dir($dir . DIRECTORY_SEPARATOR . $object) && !is_link($dir . "/" . $object)) {
            $this->rrmdir($dir . DIRECTORY_SEPARATOR . $object);
          }
          else {
            unlink($dir . DIRECTORY_SEPARATOR . $object);
          }
        }
      }

      rmdir($dir);
    }
  }

}
