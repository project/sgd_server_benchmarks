<?php

namespace Drupal\sgd_server_benchmarks;

/**
 * Class implementing the Database benchmarks.
 */
class DbBenchmarks extends Benchmarks {

  /**
   * Returns the formatted results for the DB benchmarks.
   */
  public function getResults($iterations) {

    // If the mysqli extenaion is not present then bail out.
    if (!extension_loaded('mysqli')) {
      return null;
    }

    // Whatever passed in is multiplied by 5.
    $iterations *= 5;

    $timeStart = microtime(TRUE);

    $database = \Drupal::service('database');

    $timings = self::benchmarkMySql($database->getConnectionOptions(), $iterations);

    $result = [
      'mysql_connect' => [
        'title' => $this->t('MySql connect'),
        'description' => $this->t('MySQL connect'),
        'result' => $timings['mysql_connect'],
      ],
      'mysql_select_db' => [
        'title' => $this->t('MySql select'),
        'description' => $this->t('MySQL Select DB'),
        'result' => $timings['mysql_select_db'],
      ],
      'mysql_query_version' => [
        'title' => $this->t('MySql query'),
        'description' => $this->t('Select query - SELECT VERSION() as version'),
        'result' => $timings['mysql_query_version'],
      ],
      'mysql_query_benchmark' => [
        'title' => $this->t('MySql query benchmark'),
        'description' => $this->t('Select benchmark - SELECT BENCHMARK(500000, AES_ENCRYPT(&apos;hello&apos;, UNHEX(&apos;F3229A0B371ED2D9441B830D21A390C3&apos;)))'),
        'result' => $timings['mysql_query_benchmark'],
      ],
      'total' => [
        'title' => $this->t('Total'),
        'description' => '',
        'result' => self::timerDiff($timeStart),
      ],
    ];

    return $result;
  }

  /**
   * Runs each MYSQL benchmark 'count' number of times.
   */
  private function benchmarkMySql($settings, $count) {

    $result = [
      'mysql_connect' => 0,
      'mysql_select_db' => 0,
      'mysql_query_version' => 0,
      'mysql_query_benchmark' => 0,
    ];

    for ($i = 0; $i < $count; $i++) {

      $timeStep = microtime(TRUE);
      $connection = mysqli_connect($settings['host'], $settings['username'], $settings['password'], $settings['database'], $settings['port']);
      $result['mysql_connect'] += self::timerDiff($timeStep);

      $timeStep = microtime(TRUE);
      mysqli_select_db($connection, $settings['database']);
      $result['mysql_select_db'] += self::timerDiff($timeStep);

      $timeStep = microtime(TRUE);
      $dbResult = mysqli_query($connection, 'SELECT VERSION() as version;');
      $arr_row = mysqli_fetch_array($dbResult);
      $result['mysql_query_version'] += self::timerDiff($timeStep);

      $timeStep = microtime(TRUE);
      $query = "SELECT BENCHMARK(500000, AES_ENCRYPT('hello', UNHEX('F3229A0B371ED2D9441B830D21A390C3')));";
      mysqli_query($connection, $query);
      $result['mysql_query_benchmark'] += self::timerDiff($timeStep);

      mysqli_close($connection);
    }

    return $result;
  }

}
