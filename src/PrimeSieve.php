<?php

namespace Drupal\sgd_server_benchmarks;

/**
 * Class implementing the PrimeSieve benchmark.
 */
class PrimeSieve {

  /**
   * The raw bits of the sieve.
   *
   * @var string
   */
  private string $rawbits;

  /**
   * The size of Sieve being created.
   *
   * @var int
   */
  private int $sieveSize;

  /**
   * The sieve bit size.
   *
   * @var int
   */
  private int $rawBitsSize;

  /**
   * Class constructor.
   */
  public function __construct($sieveSize = 1000000) {
    $this->sieveSize = $sieveSize;
    $this->rawBitsSize = (int) (($this->sieveSize + 1) * 0.5);
  }

  /**
   * Runs a Prime Sieve benchmark.
   */
  public function runSieve() {

    $factor = 3;
    $sieveSize = $this->sieveSize;
    $q = sqrt($sieveSize);
    $rawBitsSize = $this->rawBitsSize;
    $rb = str_repeat('0', $rawBitsSize);

    while ($factor < $q) {

      for ($i = $factor; $i <= $sieveSize; $i += 2) {

        $rbi = (int) ($i * 0.5);

        if ($rb[$rbi] == '0') {
          $factor = $i;
          break;
        }
      }

      $ft2 = $factor;
      $start = (int) ($factor * $factor * 0.5);

      for ($i = $start; $i < $rawBitsSize; $i += $ft2) {
        $rb[$i] = '1';
      }

      $factor += 2;
    }

    $this->rawbits = $rb;
  }

  /**
   * Gets the raw bit count.
   */
  public function getRawbitCount(): int {

    $sum = 0;
    $sz = strlen($this->rawbits);

    for ($i = 0; $i < $sz; $i++) {
      if ($this->rawbits[$i] == '0') {
        $sum++;
      }
    }

    return $sum;
  }

}
